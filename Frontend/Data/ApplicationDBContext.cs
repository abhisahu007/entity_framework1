﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Data
{
   public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext( DbContextOptions<ApplicationDBContext> options) : base(options)
        {

        }


      protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().Property(p => p.FirstName).HasMaxLength(50).IsRequired();
            modelBuilder.Entity<Student>().Property(p => p.LastName).HasMaxLength(50).IsRequired();
        }

        public DbSet<Student> Student { get; set; }

    }
}
