﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public int ID { get; set; }
        public string School { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
    }
}
