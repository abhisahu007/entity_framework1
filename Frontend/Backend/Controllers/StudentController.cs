﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentController : Controller
    {

        private readonly ApplicationDBContext _db;

        public StudentController(ApplicationDBContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Student Data { get; set; }

        [HttpGet]
        public Task<IEnumerable<Student>> GetAllStudent()
        {
            var data = _db.Student.Select(f => f).ToList();
            return Task.FromResult((IEnumerable<Student>)data);
        }


        [HttpPost]
        public Task<IEnumerable<Student>> AddNewStudent(Student student)
        {
            _db.Student.Add(student);
            _db.SaveChanges();
            var data = _db.Student.Select(f => f).ToList();
            return Task.FromResult((IEnumerable<Student>)data);
        }

       [HttpGet]
       [Route("/getbyid")]
        public Task<IEnumerable<Student>> GetStudentById(int id)
        {
            var data = _db.Student.Select(f => f.ID == id).ToList();
            return Task.FromResult((IEnumerable<Student>)data);
        }  
    }
}
